// Fill out your copyright notice in the Description page of Project Settings.


#include "DamageRenderComponent.h"
UDamageRenderComponent::UDamageRenderComponent(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer)
{
	PrimaryComponentTick.bCanEverTick = true;
	
}

void UDamageRenderComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction * ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	
	if (TimeToDest <= 0.f)
	{
		this->DestroyComponent(false);
	}

	FVector NewLoc = GetRelativeTransform().GetLocation() + MoveDirection * Speed * DeltaTime;

	SetRelativeLocation(NewLoc, false, nullptr, ETeleportType::None);
	TimeToDest -= DeltaTime;
	
}

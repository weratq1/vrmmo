// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/TextRenderComponent.h"
#include "DamageRenderComponent.generated.h"

/**
 * 
 */
UCLASS(ClassGroup = Rendering, hidecategories = (Object, LOD, Physics, TextureStreaming, Activation, "Components|Activation", Collision), editinlinenew, meta = (BlueprintSpawnableComponent = ""))
class VRMMO_API UDamageRenderComponent : public UTextRenderComponent
{
	GENERATED_BODY()

		UDamageRenderComponent(const FObjectInitializer & ObjectInitializer);
public:
		UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (ExposeOnSpawn))
		float Damage;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (ExposeOnSpawn))
		float TimeToDest;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (ExposeOnSpawn))
		FVector MoveDirection;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (ExposeOnSpawn))
		float Speed;

		virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction) override;
};

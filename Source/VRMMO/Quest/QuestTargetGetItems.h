// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "QuestTargetBase.h"

#include "QuestTargetGetItems.generated.h"

/**
 * 
 */


UCLASS()
class VRMMO_API UQuestTargetGetItems : public UQuestTargetBase
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int ItemID;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int Count;
	
};


// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "QuestTargetBase.h"
#include "NPC/NPC_CharacterBase.h"
#include "QuestTargetKill.generated.h"

/**
 * 
 */
UCLASS()
class VRMMO_API UQuestTargetKill : public UQuestTargetBase
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<ANPC_CharacterBase> ActorsToKill;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int Count;
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "QuestTargetBase.h"
#include "QuestAsset.generated.h"

/**
 * 
 */


USTRUCT(BlueprintType)
struct FAcceptedQuest
{
	GENERATED_BODY()

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UQuestAsset* SourceQuest;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<UQuestTargetBase*> Target;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<int> MyProgress;
};

UCLASS(BlueprintType)
class VRMMO_API UQuestAsset : public UDataAsset
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
			FText Name;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FText Discription;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FText TargetDiscription;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Instanced)
		TArray<class UQuestTargetBase*> Targets;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<UQuestAsset*> NeedToComplite;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int MinLevel;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FText DiscriptionEnd;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int RevardExp;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int RevardSilver;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<int> RevardItems;
};


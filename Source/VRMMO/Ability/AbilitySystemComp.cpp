// Fill out your copyright notice in the Description page of Project Settings.


#include "AbilitySystemComp.h"



// Sets default values for this component's properties
UAbilitySystemComp::UAbilitySystemComp()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}



// Called when the game starts
void UAbilitySystemComp::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UAbilitySystemComp::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UAbilitySystemComp::UseAbility(TSubclassOf<UAbilityBase> AbilityToUse)
{
	if (!IsValid(AbilityToUse))
	{
		return;
	}
	if (FindUsedAbilites(AbilityToUse) == -1) 
	{
		if (OwnerTags.HasAll(AbilityToUse.GetDefaultObject()->TagretMustHaveTag) && !OwnerTags.HasAny(AbilityToUse.GetDefaultObject()->BlockThisAbility))
			{
				if (IsAbilityOnCooldown(AbilityToUse) == false)
				{
					UAbilityBase* NewAb = NewObject<UAbilityBase>(this, AbilityToUse);
					OwnerTags.AppendTags(NewAb->AbilityTag);
					UsedAbilites.Add(NewAb);
					NewAb->ExecuteAbility();
					NewAb->SpendResourses();
				}	
			}
	}	
}

void UAbilitySystemComp::ApplyEffect(TSubclassOf<UAbilityEffectBase> Effect, AActor * Instigator)
{
	if(!IsValid(Effect) || Instigator == nullptr)
	{
		return;
	}

	if (!OwnerTags.HasAny(Effect.GetDefaultObject()->BlockApply))
	{
		int id = FindEffectByClass(Effect, Instigator);
		if (id != -1)
		{
			EffectsApplyed[id]->AddStuck();
			return;
		}
		UAbilityEffectBase* NewEff = NewObject<UAbilityEffectBase>(this, Effect);
		if (IsValid(NewEff))
		{
			EffectsApplyed.Add(NewEff);
			NewEff->Init(Instigator);
			OwnerTags.AppendTags(NewEff->EffectTag);
		}
	}
}

int UAbilitySystemComp::FindEffectByClass(TSubclassOf<UAbilityEffectBase> Effect, AActor* Instigator)
{
	if (IsValid(Effect) && Instigator != nullptr)
	{
	for (int i = 0; i < EffectsApplyed.Num(); ++i)
		{
			if (Effect == EffectsApplyed[i]->GetClass() && Instigator == EffectsApplyed[i]->Instigator)
			{
				return i;
			}
		}
	}	
	return -1;
}

bool UAbilitySystemComp::IsAbilityOnCooldown(TSubclassOf<UAbilityBase> Ability)
{
	if (!IsValid(Ability) || !IsValid(Ability.GetDefaultObject()->EffectCooldown))
	{
		return false;
	}
	
	return OwnerTags.HasAny(Ability.GetDefaultObject()->EffectCooldown.GetDefaultObject()->EffectTag);
}

void UAbilitySystemComp::RemoveEffectByClass(TSubclassOf<UAbilityEffectBase> Effect, AActor * Instigator)
{
	if (!IsValid(Effect) || Instigator == nullptr)
	{
		return;
	}

	int id = FindEffectByClass(Effect, Instigator);
	if(id != -1)
	{
		//UE_LOG(LogTemp, Warning, TEXT("remove"));
		OwnerTags.RemoveTags(Effect.GetDefaultObject()->EffectTag);
		EffectsApplyed.RemoveAt(id);
	}
}

void UAbilitySystemComp::RemoveAbility(TSubclassOf<UAbilityBase> Ability)
{
	if (!IsValid(Ability)) { return; }
	int  id = FindUsedAbilites(Ability);
	if (id != -1)
	{
		OwnerTags.RemoveTags(UsedAbilites[id]->AbilityTag);
		UsedAbilites.RemoveAt(id);
		
	}
}

int UAbilitySystemComp::FindUsedAbilites(TSubclassOf<UAbilityBase> Ability)
{
	for (int i = 0; i < UsedAbilites.Num(); ++i)
	{
		if (IsValid(UsedAbilites[i]))
		{
			if (Ability == UsedAbilites[i]->GetClass())
				{
					return i;
				}
		}
		else
		{
			UsedAbilites.RemoveAt(i);
		}
		
	}
	return -1;
}

void UAbilitySystemComp::EndAbilityByClass(TSubclassOf<UAbilityBase> Ability)
{
	if (!IsValid(Ability))
	{
		return;
	}
	int id = FindUsedAbilites(Ability);
	if (id != -1)
	{
		UsedAbilites[id]->EndAbility(false);
	}
}




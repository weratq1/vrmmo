// Fill out your copyright notice in the Description page of Project Settings.


#include "AbilityEffectBase.h"
#include "Ability/AbilitySystemComp.h"
#include "Engine/World.h"

UAbilityEffectBase::UAbilityEffectBase(const FObjectInitializer & ObjectInitializer)
	: Super(ObjectInitializer)
{
	Duration = StartDuration;
	EffectType = EEffectType::HasDuration;
}

void UAbilityEffectBase::BeginDestroy()
{
	Super::BeginDestroy();
}

void UAbilityEffectBase::Init(AActor* InitInstigator)
{
	Instigator = InitInstigator;
	StartEffect();

	if (EffectType == EEffectType::HasDuration)
	{
		if (TickOnStart)
		{
			ExecuteEffect();
		}
		Duration = StartDuration;
		GetWorld()->GetTimerManager().SetTimer(TickHandle, this, &UAbilityEffectBase::PrimaryTick, TickRate, true, TickRate);
		return;
	}
	if (EffectType == EEffectType::Instant)
	{
		ExecuteEffect();
		EndEffect();
		return;
	}
	if (EffectType == EEffectType::InstantWhithDuration)
	{
		ExecuteEffect();
		GetWorld()->GetTimerManager().SetTimer(TickHandle, this, &UAbilityEffectBase::EndEffect, StartDuration, false,-1.f);
	}
}

void UAbilityEffectBase::PrimaryTick()
{
	if (EffectType == EEffectType::HasDuration && Duration>0.f)
	{
		ExecuteEffect();
		Duration -= TickRate;
		return;
	}

	EndEffect();
}

void UAbilityEffectBase::EndEffect()
{
	UAbilitySystemComp * ASC = Cast<UAbilitySystemComp>(GetOuter());
	if (IsValid(ASC))
	{
		OnEffectEnd();
		GetWorld()->GetTimerManager().ClearTimer(TickHandle);
		ASC->RemoveEffectByClass(this->GetClass(), IsValid(Instigator) ? Instigator : GetOwner());
		MarkPendingKill();
	}
}

void UAbilityEffectBase::AddStuck()
{ 
	if (CurrStuck < MaxStack)
	{
		CurrStuck++;
		AddStuck();
		if (RefreshDuration)
		{
			Duration = StartDuration;
		}
		return;
	}
	//if can't add new stack refresh time
	Duration = StartDuration;
}

AActor * UAbilityEffectBase::GetOwner()
{
	UAbilitySystemComp * ASC = Cast<UAbilitySystemComp>(GetOuter());
	if (IsValid(ASC))
	{
		return ASC->GetOwner();
	}
	return nullptr;
}

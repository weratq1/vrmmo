// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "GameplayTagContainer.h"
#include "Ability/AbilityEffectBase.h"
#include "GameFramework/DamageType.h"
#include "AbilityBase.generated.h"

/**
 * 
 */
USTRUCT(BlueprintType)
struct FOnMeleeDamage
{
	GENERATED_BODY()

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			float Damage;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<UDamageType> NewDamageType;

};

UCLASS(Blueprintable)
class VRMMO_API UAbilityBase : public UObject
{
	GENERATED_BODY()
public:
		UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
			FGameplayTagContainer AbilityTag;
		UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
			FGameplayTagContainer BlockThisAbility;
		UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
			FGameplayTagContainer TagretMustHaveTag; 
		UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
			FGameplayTagContainer SourceMustHaveAbility;
		UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
			TSubclassOf<UAbilityEffectBase> EffectCooldown;
		UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
			int Priority;


		UFUNCTION(BlueprintImplementableEvent)
			void ExecuteAbility();
		UFUNCTION(BlueprintImplementableEvent)
			void OnEndAbility(bool bForce);
		UFUNCTION(BlueprintCallable)
			void CommitAbility();
		UFUNCTION(BlueprintCallable)
			AActor* GetOwner();
		UFUNCTION(BlueprintImplementableEvent)
			void SpendResourses();
		UFUNCTION(BlueprintCallable)
			void EndAbility(bool bForce);
		UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
			FOnMeleeDamage OnMelleWeaponAttack(AActor* Weapon, AActor* Target, float WeaponStrength, float WeaponDamage, TSubclassOf<UDamageType> DamageType);

};

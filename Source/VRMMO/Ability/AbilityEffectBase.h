// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "GameplayTagContainer.h"
#include "TimerManager.h"
#include "AbilityEffectBase.generated.h"

/**
 * 
 */
UENUM(BlueprintType)
enum class EEffectType : uint8
{
	Instant,
	HasDuration,
	InstantWhithDuration
};

UCLASS(Blueprintable)
class VRMMO_API UAbilityEffectBase : public UObject
{
	GENERATED_BODY()

		UAbilityEffectBase(const FObjectInitializer & ObjectInitializer);
public:

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		FGameplayTagContainer EffectTag;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		FGameplayTagContainer ApplyTagToSource;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		FGameplayTagContainer BlockApply;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		EEffectType EffectType;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		float StartDuration;
	UPROPERTY(BlueprintReadWrite)
		float Duration;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		float TickRate;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		bool TickOnStart;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		bool Stack;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (EditCondition = Stack))
		int MaxStack;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (EditCondition = Stack))
		bool RefreshDuration;
	UPROPERTY(BlueprintReadWrite)
		AActor* Instigator;
	UPROPERTY(BlueprintReadWrite)
		int CurrStuck;
	UFUNCTION(BlueprintImplementableEvent)
		void StartEffect();
	UFUNCTION(BlueprintImplementableEvent)
		void OnAddStuck();
	
	virtual void BeginDestroy() override;
	
	void Init(AActor* InitInstigator);
	void PrimaryTick();
	
	FTimerHandle TickHandle;
	UFUNCTION(BlueprintImplementableEvent)
		void ExecuteEffect();
	UFUNCTION(BlueprintImplementableEvent)
		void OnEffectEnd();
	UFUNCTION(BlueprintCallable)
		void AddStuck();
	UFUNCTION(BlueprintCallable)
		AActor* GetOwner();
	UFUNCTION(BlueprintCallable)
		void EndEffect();
};

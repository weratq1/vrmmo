// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Ability/AbilityBase.h"
#include "AbilitySystemComp.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnAbilityEnd, TSubclassOf<UAbilityEffectBase>, AbilityClass);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class VRMMO_API UAbilitySystemComp : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UAbilitySystemComp();
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		FGameplayTagContainer OwnerTags;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		TArray<UAbilityBase*> UsedAbilites;
	UPROPERTY(BlueprintReadWrite)
		TArray<UAbilityEffectBase*> EffectsApplyed;

	UFUNCTION(BlueprintCallable)
		void UseAbility(TSubclassOf<UAbilityBase> AbilityToUse);
	UFUNCTION(BlueprintCallable)
		void ApplyEffect(TSubclassOf<UAbilityEffectBase> Effect, AActor* Instigator);
	UFUNCTION(BlueprintCallable)
		int FindEffectByClass(TSubclassOf<UAbilityEffectBase> Effect, AActor* Instigator);
	UFUNCTION(BlueprintCallable)
		bool IsAbilityOnCooldown(TSubclassOf<UAbilityBase> Ability);
	UFUNCTION(BlueprintCallable)
		void RemoveEffectByClass(TSubclassOf<UAbilityEffectBase> Effect, AActor* Instigator);
	UFUNCTION(BlueprintCallable)
		void RemoveAbility(TSubclassOf<UAbilityBase> Ability);
	UFUNCTION(BlueprintCallable)
		int FindUsedAbilites(TSubclassOf<UAbilityBase> Ability); 
	UFUNCTION(BlueprintCallable)
		void EndAbilityByClass(TSubclassOf<UAbilityBase> Ability);
	
	UPROPERTY(BlueprintAssignable)
		FOnAbilityEnd AilityEnd;
	

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};

// Fill out your copyright notice in the Description page of Project Settings.


#include "AbilityBase.h"
#include "Ability/AbilitySystemComp.h"

void UAbilityBase::CommitAbility()
{
	if (!IsValid(EffectCooldown)) { return; }
	UAbilitySystemComp* ASC = Cast<UAbilitySystemComp>(GetOuter());
	if (IsValid(ASC))
	{
		ASC->ApplyEffect(EffectCooldown,ASC->GetOwner());
	}
}

AActor * UAbilityBase::GetOwner()
{
	UAbilitySystemComp * ASC = Cast<UAbilitySystemComp>(GetOuter());
	if (IsValid(ASC))
	{
		return ASC->GetOwner();
	}
	return nullptr;
}



void UAbilityBase::EndAbility(bool bForce)
{
	UAbilitySystemComp * ASC = Cast<UAbilitySystemComp>(GetOuter());
	if (IsValid(ASC))
	{
		ASC->RemoveAbility(this->GetClass());
	}
	OnEndAbility(false);
	MarkPendingKill();
}

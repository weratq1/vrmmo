// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Engine/Texture2D.h"
#include "Engine/StaticMesh.h"
#include "Engine/DataTable.h"
#include "ItemData.generated.h"

/**
 * 
 */
UENUM(BlueprintType)
enum class EWeaponType : uint8
{
	//GENERATED_BODY()

	WT_OneHandSword UMETA(DisplayName = "OneHandSword"),
	WT_TwoHandSword UMETA(DisplayName = "TwoHandSword"),
	WT_LeftHand UMETA(DisplayName = "LeftHand"),
	WT_Bow UMETA(DisplayName = "Bow"),
	WT_Staff UMETA(DisplayName = "Staff")

};

UENUM(BlueprintType)
enum class EItemType : uint8
{
	//GENERATED_BODY()

	IT_Other UMETA(DisplayName = "Other"),
	IT_Head UMETA(DisplayName = "Head"),
	IIT_Cloak UMETA(DisplayName = "Cloak"),
	T_Shoulders UMETA(DisplayName = "Shoulders"),
	IT_Chest UMETA(DisplayName = "Chest"),
	IT_Glovers UMETA(DisplayName = "Glovers"),
	IT_Shoes UMETA(DisplayName = "Shoes"), 
	IT_Boots UMETA(DisplayName = "Boots"),
	IT_OneHand UMETA(DisplayName = "OneHand"),
	IT_TwoHand UMETA(DisplayName = "TwoHand"),
	IT_LeftHand UMETA(DisplayName = "LeftHand")

};

UENUM(BlueprintType)
enum class EItemParams : uint8
{	
	IP_Attack UMETA(DisplayName = "Attack"),
	IP_Crit UMETA(DisplayName = "Crit"),
	IP_Block UMETA(DisplayName = "Block"),
	IP_Health UMETA(DisplayName = "Health"),
	IP_Mana UMETA(DisplayName = "Mana"),
	IP_Strength UMETA(DisplayName = "Strength"),
	IP_Agility UMETA(DisplayName = "Agility"),
	IP_Intelligence UMETA(DisplayName = "Intelligence"),
	IP_SpellPower UMETA(DisplayName = "SpellPower"),
	IP_Armor UMETA(DisplayName = "Armor"),
	IP_WalkSpeed UMETA(DisplayName = "WalkSpeed"),
	IP_MagicResist UMETA(DisplayName = "MagicResist"),
	IP_IgnoreArmor UMETA(DisplayName = "IgnoreArmor"),
	IP_Spirit UMETA(DisplayName = "Spirit")
};


UENUM(BlueprintType)
enum class EItemQuality : uint8
{
	

	EQ_Common UMETA(DisplayName = "Common"),
	EQ_UnCommon UMETA(DisplayName = "UnCommon"),
	EQ_Rare UMETA(DisplayName = "Rare"),
	EQ_Epic UMETA(DisplayName = "Epic"),
	EQ_Legendary UMETA(DisplayName = "Legendary"),
	EQ_Artifact UMETA(DisplayName = "Artifact")

};

USTRUCT(BlueprintType)
struct FItemStatData
{
	GENERATED_BODY()
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		EItemParams Param;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int Value;

};

USTRUCT(BlueprintType)
struct FItemInfoData : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FText Name;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FText Discription;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		EItemType ItemType;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		EItemQuality Quality;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UTexture2D* Icon;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UStaticMesh* Mesh;	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool bIsStuck;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int MaxStuck;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<FItemStatData> ItemParams;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int Price;
};

USTRUCT(BlueprintType)
struct FInventorySlot
{
	GENERATED_BODY()
		UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FItemInfoData Item;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int Count;

};

UENUM(BlueprintType)
enum class EQuestTask : uint8
{
	QT_Kill UMETA(DisplayName = "Kill"),
	QT_Talk UMETA(DisplayName = "Talk"),
	QT_Get UMETA(DisplayName = "Get")

};




USTRUCT(BlueprintType)
struct FQuestTaskKill
{
	GENERATED_BODY()
		UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<AActor> ActorToKill;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int Count;
};

USTRUCT(BlueprintType)
struct FQuestTaskGet
{
	GENERATED_BODY()
		UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int ItemToGiveID;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int Count;
};

USTRUCT(BlueprintType)
struct FQuestTask
{
	GENERATED_USTRUCT_BODY()
public:
	FQuestTask()
		: Task(EQuestTask::QT_Kill)
	{
	}
	FQuestTask(const FQuestTaskKill& Value)
		: Task(EQuestTask::QT_Kill)
		, KillTask (Value)
	{
	}
	FQuestTask(const FQuestTaskGet& Value)
		: Task(EQuestTask::QT_Get)
		, GetTask(Value)
	{
	}	
	FQuestTask(const TSubclassOf<AActor> Value)
		: Task(EQuestTask::QT_Talk)
		, ActorToTalk(Value)
	{
	}

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		EQuestTask Task;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int Count;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FQuestTaskKill KillTask;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FQuestTaskGet GetTask;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<AActor> ActorToTalk;
};

USTRUCT(BlueprintType)
struct FQuestData
{
	GENERATED_BODY()



	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FText Name;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FText Discription;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FText ShotTask;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<FQuestTask> Tasks;
};




UCLASS()
class VRMMO_API UItemData : public UObject
{
	GENERATED_BODY()
	
public:
	
};

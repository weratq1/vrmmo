// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "Quest/QuestAsset.h"
#include "MyPlayerState.generated.h"

/**
 * 
 */
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnRepAccQue);

UCLASS()
class VRMMO_API AMyPlayerState : public APlayerState
{
	GENERATED_BODY()
		virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
public:
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite)
		TArray<UQuestAsset*> ComplitedQuest;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, ReplicatedUsing = OnRep_AcceptedQest)
		TArray<FAcceptedQuest> AcceptedQuest;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, ReplicatedUsing = OnRep_AcceptedQest)
		TArray<UQuestAsset*> ReadyToEndQuest;
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite)
		int PlayerLevel;
	UFUNCTION()
		virtual void OnRep_AcceptedQest();
	UPROPERTY(BlueprintAssignable)
	FOnRepAccQue OnRepAccQuest;
};

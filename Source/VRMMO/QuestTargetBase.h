// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "QuestTargetBase.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, Abstract, EditInlineNew, CollapseCategories)
class VRMMO_API UQuestTargetBase : public UObject
{
	GENERATED_BODY()

};

// Fill out your copyright notice in the Description page of Project Settings.


#include "MyPlayerState.h"
#include "UnrealNetwork.h"

void AMyPlayerState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AMyPlayerState, ComplitedQuest);
	DOREPLIFETIME(AMyPlayerState, AcceptedQuest);
	DOREPLIFETIME(AMyPlayerState, PlayerLevel);
	DOREPLIFETIME(AMyPlayerState, ReadyToEndQuest);
}

void AMyPlayerState::OnRep_AcceptedQest()
{
	
	OnRepAccQuest.Broadcast();
}
